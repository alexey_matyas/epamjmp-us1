import com.epam.cdp.SimpleSyntaxHighlighter;
import com.epam.cdp.SyntaxHighlighter;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alexey on 27.03.2016.
 */
public class SimpleSyntaxHighlighterTest {
    private SyntaxHighlighter underTest;

    @Before
    public void before() throws IOException {
        underTest = new SimpleSyntaxHighlighter(System.getProperty("user.dir")+"\\highlight.properties");
    }

    @Test
    public void testConfiguredWordsWrappedWithTag() {
        assertEquals(
                underTest.highlight("I am going to join java mentoring program to learn cool stuff in fun way"),
                "I [bold]am[/bold] going [italic][bold]to[/bold][/italic] join java mentoring program [italic][bold]to[/bold][/italic] learn cool stuff [underline]in[/underline] fun way"
        );
    }

    @Test
    public void testTextWithoutConfiguredWordsUnchanged() {
        assertEquals(
                underTest.highlight("Bla bla bla."),
                "Bla bla bla."
        );
    }
}
