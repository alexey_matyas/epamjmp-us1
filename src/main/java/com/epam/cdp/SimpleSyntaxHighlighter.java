package com.epam.cdp;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Alexey on 26.03.2016.
 */
public class SimpleSyntaxHighlighter implements SyntaxHighlighter {
    Map<String, Set<String>> highlightRules;

    public SimpleSyntaxHighlighter(String rulesFilePath) throws IOException {
        Properties props = new Properties();
        highlightRules = new HashMap<String, Set<String>>();

        props.load(new FileReader(rulesFilePath));

        for(String tag : props.stringPropertyNames()) {
            for(String word : props.getProperty(tag).split(",")) {
                if(!highlightRules.containsKey(word)) {
                    highlightRules.put(word, new HashSet<String>(Arrays.asList(tag)));
                } else {
                    highlightRules.get(word).add(tag);
                }
            }
        }
    }

    public String highlight(String theCompleteSentence) throws UnableToHighlightException {
        Scanner sc = new Scanner(theCompleteSentence);
        StringBuffer sb = new StringBuffer();

        while (sc.hasNext()) {
            String word = sc.next();

            if(highlightRules.containsKey(word)) {
                for(String tag : highlightRules.get(word)) {
                    word = tagWord(word, tag);
                }
            }

            if(sc.hasNext()) {
                word += " ";
            }

            sb.append(word);
        }

        return sb.toString();
    }

    private String tagWord(String word, String tag) {
        return "[" + tag + "]" + word + "[/" + tag + "]";
    }
}
