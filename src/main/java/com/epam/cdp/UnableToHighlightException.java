package com.epam.cdp;

public class UnableToHighlightException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public UnableToHighlightException(String message) {
		super(message);
	}
	
	public UnableToHighlightException(String message, Throwable reason) {
		super(message, reason);
	}
 
	
}
