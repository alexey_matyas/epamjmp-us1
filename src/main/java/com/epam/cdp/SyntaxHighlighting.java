package com.epam.cdp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Alexey on 26.03.2016.
 */
public class SyntaxHighlighting {
    private static final String WORKING_DIR = System.getProperty("user.dir");
    private static final String DEFAULT_INPUT_PATH = WORKING_DIR + "\\input.txt";
    private static final String DEFAULT_RULES_PATH = WORKING_DIR + "\\highlight.properties";

    public static void main(String[] args) throws IOException {
        String inputPath = args.length > 0 ? args[0] : DEFAULT_INPUT_PATH;
        String rulesPath = args.length > 1 ? args[1] : DEFAULT_RULES_PATH;

        SyntaxHighlighter highlighter = new SimpleSyntaxHighlighter(rulesPath);
        byte[] bytes = Files.readAllBytes(Paths.get(inputPath));

        System.out.println(highlighter.highlight(new String(bytes)));
    }
}
