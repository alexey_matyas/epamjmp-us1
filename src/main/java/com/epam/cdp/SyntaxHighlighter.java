package com.epam.cdp;

public interface SyntaxHighlighter {
	
	String highlight(String theCompleteSentence) throws UnableToHighlightException;

}
